// TEST
digraph G {
	graph [rankdir=TD]
	subgraph cluster_world {
		label=world
		subgraph "cluster_world.pump" {
			label="world.pump"
			"world.pump.convert-torque" [shape=ellipse]
		}
		subgraph "cluster_world.drive-mechanism" {
			label="world.drive-mechanism"
			"world.drive-mechanism.convert-power-potential" [shape=ellipse]
		}
		"world.provide-torque" [shape=hexagon]
	}
	"world.drive-mechanism.convert-power-potential" -> "world.provide-torque" [style=solid]
	"world.provide-torque" -> "world.pump.convert-torque" [style=solid]
}

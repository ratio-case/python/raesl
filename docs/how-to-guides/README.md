# How-to guides

Hello! This section is made to hold how-to guides, which are pages dedicated to solving the
question:

> "How do I...?

It will be populated as we go! Feel free to browse the examples in the menu on the left or pick one
of:

- [Basic usage](./basic.md) to get going with parsing and compiling your ESL specification.
- [Document generation](./doc.md) to generate display documents from your ESL specification.
- [Excel](./excel.md) to generate Excel workbooks from your ESL specification.
- [Plotting and visualization](./plot.md) instructions to generate matrix figures and functional
  traceability diagrams from your ESL specification!
